FROM debian:buster-slim AS schemas

RUN apt-get update && apt-get install -y git && \
    git clone https://gitlab.com/gitlab-org/security-products/analyzers/integration-test.git /integration-test && \
    /integration-test/scripts/export_security-report-schemas_dist.sh /integration-test/security-report-schemas

FROM registry.gitlab.com/gitlab-org/security-products/analyzers/gemnasium:latest

COPY --from=schemas /integration-test /integration-test

RUN apk add --no-cache docker ruby ruby-bundler ruby-json ruby-bigdecimal git ruby-dev make gcc musl-dev libc6-compat curl && \
    mkdir /go && \
    curl -L https://golang.org/dl/go1.17.6.linux-amd64.tar.gz -o /go/go1.17.6.linux-amd64.tar.gz && \
    tar -C /usr/local -xzf /go/go1.17.6.linux-amd64.tar.gz

ENV SECURITY_REPORT_SCHEMAS_DIR="/integration-test/security-report-schemas"
ENV PATH="/usr/local/go/bin:${PATH}"

WORKDIR /integration-test

RUN gem build *.gemspec
RUN gem install ./*.gem
RUN rspec

RUN cp scripts/dependency-scanning-qa.rb /usr/local/bin/dependency-scanning-qa
RUN cp scripts/secret-detection-qa.rb /usr/local/bin/secret-detection-qa
RUN cp scripts/sast-qa.rb /usr/local/bin/sast-qa
